<?php
	session_start();
	if (!isset($_SESSION['username'])) {
	header('Location: login.php');
	}
	session_destroy();
	unset($_SESSION['username']);
	$_SESSION['message']="You are logged out";
	header("location:login.php");
?>