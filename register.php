<?php
session_start();
//connect to database
$db=mysqli_connect("localhost","root","420024","user_login");
if(isset($_POST['register_btn']))
{
      $username = mysqli_real_escape_string($db, $_POST['username']);
      $email = mysqli_real_escape_string($db, $_POST['email']);
      $password = mysqli_real_escape_string($db, $_POST['password']);
      $password2 = mysqli_real_escape_string($db, $_POST['password2']);
      
      $u="SELECT username FROM login WHERE username='$username'";
      $p=mysqli_query($db,$u);
	    
      $e="SELECT email FROM login WHERE email='$email'";
      $q=mysqli_query($db,$e);
      
      if($password==$password2 && mysqli_num_rows($p)==0 && mysqli_num_rows($q)==0)
     {           //Create User
            $password=md5($password); //hash password before storing for security purposes
	    $sql="INSERT INTO login(username,passwd,email) VALUES('$username','$password','$email')";
            mysqli_query($db,$sql);
            $_SESSION['message']="You are now logged in";
            $_SESSION['username']=$username;
            header("location:home.php");  //redirect home page
      }

    elseif(mysqli_num_rows($p)>0)
	{
		$_SESSION['message']="Username already exist"; 	
	}
    elseif(mysqli_num_rows($q)>0)
	{
		$_SESSION['message']="E-mail already exist"; 	
	}
    else
	{
      		$_SESSION['message']="The two password do not match";   
        }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Register , login and logout user php mysql</title>
  <link rel="stylesheet" type="text/css" href="style.css"/>

  <script type='text/javascript' src='http://code.jquery.com/jquery-latest.min.js'></script>
  <script>
	function checkforblank()
	{   //alert("function called");
		if(document.getElementById('username').value =="")
		{
			alert('please enter username, username cant be empty');
			document.getElementById('username').style.borderColor="red";
			return false;
		}
		if(document.getElementById('email').value =="")
		{
			alert('please enter email,email cant be empty');
			document.getElementById('email').style.borderColor="red";
			return false;
		}
		if(document.getElementById('password').value =="")
		{
			alert('please enter password,password cant be empty');
			document.getElementById('password').style.borderColor="red";
			return false;
		}
	}
  </script>


</head>
<body>
<div class="header">
    <h1>New User Registration</h1>
</div>
<?php
    if(isset($_SESSION['message']))
    {
         echo "<div id='error_msg'>".$_SESSION['message']."</div>";
    }
?>
<form method="post" action="register.php" onsubmit="return checkforblank()">
  <table>
     <tr>
           <td>Username : </td>
           <td><input type="text" id="username" name="username" class="textInput"></td>
     </tr>
     <tr>
           <td>Email : </td>
           <td><input type="email" id="email" name="email" class="textInput"></td>
     </tr>
      <tr>
           <td>Password : </td>
           <td><input type="password" id="password" name="password" class="textInput"></td>
     </tr>
      <tr>
           <td>Password again: </td>
           <td><input type="password" name="password2" class="textInput"></td>
     </tr>
      <tr>
           <td></td>
           <td><input type="submit" name="register_btn" class="Register" value="Submit"></td>
     </tr>
 
</table>
</form>
<br><br>
<a href='login.php'><p style="text-align:center">Returning user login</p></a>
</body>
</html>
